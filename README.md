# TBX-ST-RDFLifter #

TBX-ST-RDFLifter is an RDF lifting extension for [Semantic Turkey](https://bitbucket.org/art-uniroma2/semantic-turkey/) that enables transformation of data serialized in the TBX format into RDF Data.

The extension is based on the [TBX2RDF](https://github.com/manuelfiorelli/tbx2rdf) utility, developed by Manuel Fiorelli and Andrea Turbati as a fork of the original [TBX2RDF](https://github.com/cimiano/tbx2rdf)

## Building the project ##

### Requirements ###
1. a properly installed JDK version 21
2. a recent version of Apache Maven, preferably version 3.9.5 or superior (see section troubleshooting below)

### Building ###

1. Build the [tbx2rdf fork](https://github.com/manuelfiorelli/tbx2rdf) with a plain:
   mvn clean install
   which will deploy the tbx2rdf jar into your local Maven repository
   
2. build this project with:
   mvn clean install   
   this will also embed tbx2rdf into the built jar bundle of the lifter
   
3. place the built tbx-st-rdflifter-<ver>.jar plugin into the `plugins` directory of Semantic Turkey

