package it.uniroma2.art.showvoc.tbx;

import java.util.Arrays;
import java.util.List;

import it.uniroma2.art.semanticturkey.extension.NonConfigurableExtensionFactory;
import it.uniroma2.art.semanticturkey.extension.extpts.commons.io.FormatCapabilityProvider;
import it.uniroma2.art.semanticturkey.resources.DataFormat;
import org.springframework.stereotype.Component;

@Component
public class TBXRDFLifterFactory implements NonConfigurableExtensionFactory<TBXRDFLifter>, FormatCapabilityProvider {

	@Override
	public String getName() {
		return "TBX RDF Lifter";
	}

	@Override
	public String getDescription() {
		return "An RDF Lifter that converts TBX terminologies to RDF data using the OntoLex-Lemon vocabulary";
	}

	@Override
	public TBXRDFLifter createInstance() {
		return new TBXRDFLifter();
	}

	@Override
	public List<DataFormat> getFormats() {
		return List.of(new DataFormat("TBX", "application/x-tbx", "tbx"));
	}

}
