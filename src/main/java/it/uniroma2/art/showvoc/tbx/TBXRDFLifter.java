package it.uniroma2.art.showvoc.tbx;

import it.uniroma2.art.semanticturkey.extension.extpts.rdflifter.LifterContext;
import it.uniroma2.art.semanticturkey.extension.extpts.rdflifter.LiftingException;
import it.uniroma2.art.semanticturkey.extension.extpts.rdflifter.RDFLifter;
import it.uniroma2.art.semanticturkey.extension.extpts.reformattingexporter.ClosableFormattedResource;
import it.uniroma2.art.semanticturkey.extension.extpts.urigen.URIGenerationException;
import it.uniroma2.art.semanticturkey.project.Project;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFHandler;
import org.eclipse.rdf4j.rio.RDFParser;
import org.eclipse.rdf4j.rio.Rio;
import org.eclipse.rdf4j.rio.UnsupportedRDFormatException;
import org.eclipse.rdf4j.rio.helpers.StatementCollector;
import org.eclipse.rdf4j.rio.ntriples.NTriplesParserSettings;
import tbx2rdf.Mappings;
import tbx2rdf.TBX2RDF_Converter;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class TBXRDFLifter implements RDFLifter {
	private Mappings mappings;

	public TBXRDFLifter() {
		try {

			try (Reader reader = new InputStreamReader(
					this.getClass().getResourceAsStream("mappings.default"))) {
				mappings = Mappings.readInMappings(reader);
			}

		} catch (IOException e) {
			throw new RuntimeException("Unable to instantiate " + TBXRDFLifter.class.getSimpleName(), e);
		}

	}

	@Override
	public void lift(ClosableFormattedResource sourceFormattedResource, String format,
			RDFHandler targetRDFHandle, LifterContext lifterContext) throws LiftingException, IOException {
		TBX2RDF_Converter tbxConverter = new TBX2RDF_Converter();
		Path tempOutputFile = Files.createTempFile("tbxrdflifter", "nt");
		try {
			try (PrintStream fos = new PrintStream(tempOutputFile.toFile(), "UTF-8")) {
				tbxConverter.convertAndSerializeLargeFile(
						sourceFormattedResource.getBackingFile().getAbsolutePath(), fos,
						lifterContext.getDefaultNamespace(), mappings);
			}

			try (Reader fos = new InputStreamReader(new FileInputStream(tempOutputFile.toFile()),
					StandardCharsets.UTF_8)) {
				RDFParser rdfParser = Rio.createParser(RDFFormat.NTRIPLES);
				rdfParser.getParserConfig().set(NTriplesParserSettings.FAIL_ON_INVALID_LINES, false);
				rdfParser.setRDFHandler(targetRDFHandle);
				rdfParser.parse(fos, "");
			}
		} finally {
			Files.delete(tempOutputFile);
		}
	}

	public static void main(String[] args)
			throws UnsupportedRDFormatException, LiftingException, IOException {
		List<Statement> statements = new ArrayList<>();
		File tbxFile = new File(
				"C:/Users/Manuel/WORK/ART/Workspace/Eclipse/tbx2rdf/doc/reference/TBX-resources/maryland.tbx");
		ClosableFormattedResource tbxResource = new ClosableFormattedResource(tbxFile, "tbx",
				"application/x-tbx", StandardCharsets.UTF_8, "maryland.tbx");
		TBXRDFLifter tbxLifter = new TBXRDFLifter();
		tbxLifter.lift(tbxResource, "TBX", new StatementCollector(statements), new LifterContext() {

			@Override
			public IRI getLexicalizationModel() {
				return Project.RDFS_LEXICALIZATION_MODEL;
			}

			@Override
			public String getDefaultNamespace() {
				return "http://example.org/";
			}

			@Override
			public IRI generateIRI(String xRole, Map<String, Value> valueMapping)
					throws URIGenerationException {
				return SimpleValueFactory.getInstance().createIRI(getDefaultNamespace(),
						UUID.randomUUID().toString());
			}
		});

		Rio.write(statements, Rio.createWriter(RDFFormat.NTRIPLES, System.out));

	}

}
