package it.uniroma2.art.showvoc.tbx;

import it.uniroma2.art.semanticturkey.pf4j.STPlugin;
import org.pf4j.PluginWrapper;

public class TBXRDFLifterPlugin extends STPlugin {
    public TBXRDFLifterPlugin(PluginWrapper wrapper) {
        super(wrapper);
    }
}
